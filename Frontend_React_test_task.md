# Test Task - Frontend React.js developer

Hi! If you got here then we need you! We have prepared test task for you to check your code skills. Please look throught all points carefully (if you think that some of the requirements are overkill for such an app -> you are probably right but we need to check your knowladge of it so just face it :)
Please share the link of your repo with us when you finish.

### Goal

Create an SPA that shows the weather

---

### Requirements

1. By default app should show user's weather by his **coordinates**
2. User has ability to **search** city by name
3. User could **add/delete cities**, so you need to show *separate weather widget* per city
4. **Store data** locally
5. Your App should have at least **3** test cases (take care also of testing **Redux**)

---

### Technologies

1. **React.js** as a core
2. **Redux** for data storing
3. **Middleware** at your discretion (mostly we work with *redux-saga*)
4. As a *Component library* please use either **Material-UI** or **React Bootstrap** (mostly we work with *Material-UI*)

---

### Design
Do it at your discretion. We have just a few points here:
1. Keep it simple and user friendly
2. Make it responsive (from **365px** up )
3. Use *autocomplete* for search intput, so by click on city from dropdown new widget with that city will be added

---

### Recommendations
1. You could use [openweathermap](https://openweathermap.org) or any other service with public API
2. Use **Hooks**
3. Use **CSS-Modules**
